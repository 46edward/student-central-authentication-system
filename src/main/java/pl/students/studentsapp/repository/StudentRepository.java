package pl.students.studentsapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.students.studentsapp.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByEmail(String email);
    void deleteStudentByEmail(String email);
}
