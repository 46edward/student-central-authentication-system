package pl.students.studentsapp.repository;

import org.springframework.stereotype.Repository;
import pl.students.studentsapp.model.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Repository
public class InMemoryStudentsDAO {

    private final List<Student> STUDENTS = new ArrayList<Student>();

    public List<Student> findAllLocalStudents(){
        return this.STUDENTS;
    }

    public Student storeLocalStudent(Student student) {
        if(student != null){
            STUDENTS.add(student);
        } else {
            throw new NullPointerException("storeStudent - student is null");
        }
        return student;
    }

    public Student findLocalByEmail(String email) {
       return STUDENTS.stream()
               .filter(student -> student.getEmail().equals(email))
               .findFirst()
               .orElse(null);
    }

    public Student updateLocalStudent(Student student) {
        int index = IntStream.range(0, STUDENTS.size())
                .filter(i -> STUDENTS.get(i).getEmail().equals(student.getEmail()))
                .findFirst()
                .orElse(-1);

        if(index > -1) {
            STUDENTS.set(index, student);
            return student;
        }

        return null;
    }

    public void deleteLocalStudent(String email) {
        if(findLocalByEmail(email) != null){
            STUDENTS.remove(findLocalByEmail(email));
        }
    }

}
