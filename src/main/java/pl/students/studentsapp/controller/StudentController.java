package pl.students.studentsapp.controller;

import org.springframework.web.bind.annotation.*;
import pl.students.studentsapp.model.Student;
import pl.students.studentsapp.service.StudentService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

    private final StudentService service;

    public StudentController(StudentService service) {
        if(service != null){
            this.service = service;
        } else {
            throw new NullPointerException("Service in StudentController is null");
        }
    }

    @GetMapping
    public List<Student> findAllStudents(){
        return service.findAllStudents();
    }

    @PostMapping
    public Student storeStudent(@RequestBody Student student){
        return service.storeStudent(student);
    }

    @GetMapping("/{email}")
    public Student findByEmail(@PathVariable String email){
        return service.findByEmail(email);
    }

    @PutMapping
    public Student updateStudent(@RequestBody Student student){
        return service.updateStudent(student);
    }

    @DeleteMapping("/{email}")
    public void deleteStudent(@PathVariable String email){
        service.deleteStudent(email);
    }

}
