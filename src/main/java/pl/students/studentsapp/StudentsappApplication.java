package pl.students.studentsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class StudentsappApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsappApplication.class, args);
	}

}
