package pl.students.studentsapp.service.Impl;

import jakarta.transaction.Transactional;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.students.studentsapp.model.Student;
import pl.students.studentsapp.repository.StudentRepository;
import pl.students.studentsapp.service.StudentService;

import java.util.List;

@Primary
@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository service;

    public StudentServiceImpl(StudentRepository service) {
        this.service = service;
    }

    @Override
    public List<Student> findAllStudents() {
        return service.findAll();
    }

    @Override
    public Student storeStudent(Student student) {
        return service.save(student);
    }

    @Override
    public Student findByEmail(String email) {
        return service.findByEmail(email);
    }

    @Override
    public Student updateStudent(Student student) {
        return service.save(student);
    }

    @Transactional
    @Override
    public void deleteStudent(String email) {
        service.deleteStudentByEmail(email);
    }
}
