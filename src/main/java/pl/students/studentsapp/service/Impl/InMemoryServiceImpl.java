package pl.students.studentsapp.service.Impl;

import org.springframework.stereotype.Service;
import pl.students.studentsapp.model.Student;
import pl.students.studentsapp.repository.InMemoryStudentsDAO;
import pl.students.studentsapp.service.StudentService;

import java.util.List;

@Service
public class InMemoryServiceImpl implements StudentService {

    private final InMemoryStudentsDAO memoryStudents;

    public InMemoryServiceImpl(InMemoryStudentsDAO memoryStudents) {
        this.memoryStudents = memoryStudents;
    }

    @Override
    public List<Student> findAllStudents(){
        return memoryStudents.findAllLocalStudents();
    }

    @Override
    public Student storeStudent(Student student) {
        return memoryStudents.storeLocalStudent(student);
    }

    @Override
    public Student findByEmail(String email) {
        return memoryStudents.findLocalByEmail(email);
    }

    @Override
    public Student updateStudent(Student student) {
        return memoryStudents.updateLocalStudent(student);
    }

    @Override
    public void deleteStudent(String email) {
        memoryStudents.deleteLocalStudent(email);
    }
}
